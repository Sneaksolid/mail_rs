use super::error;
use super::helper_rs;
use super::imap_client;
use super::Command;

pub struct Fetch;

impl Fetch {
    pub fn new() -> Fetch {
        Fetch {}
    }
}

impl Command for Fetch {
    fn name<'a>(&self) -> &'a str {
        "fetch"
    }

    fn execute(
        &self,
        imap_client: &mut imap_client::ImapClient,
        params: &Vec<String>,
    ) -> Result<(), error::CommandError> {
        if let Some(arg) = params.get(0) {
            let lines_to_fetch: u32;
            if let Ok(v) = arg.parse::<u32>() {
                lines_to_fetch = v;
            } else {
                lines_to_fetch = 10;
            }

            match imap_client.print_most_recent_headers(lines_to_fetch) {
                Ok(_) => return Ok(()),
                Err(e) => return Err(error::CommandError::new_from_error(Box::new(e))),
            };
        }

        Ok(())
    }
}

pub struct Read;

impl Read {
    pub fn new() -> Read {
        Read {}
    }
}

impl Command for Read {
    fn name<'a>(&self) -> &'a str {
        "read"
    }

    fn execute(
        &self,
        imap_client: &mut imap_client::ImapClient,
        params: &Vec<String>,
    ) -> Result<(), error::CommandError> {
        if params.len() != 1 {
            return Err(error::CommandError::new(String::from(
                "Wrong number of arguments",
            )));
        }

        let mut read_id: u32 = 1;
        if let Some(var) = params.get(0) {
            if let Ok(id) = var.parse::<u32>() {
                read_id = id;
            } else {
                return Err(error::CommandError::new(String::from("Could not parse id")));
            }
        }

        match imap_client.get_mail_text(read_id) {
            Ok(text) => match helper_rs::terminal::show_text_fancy(&text) {
                Ok(_) => return Ok(()),
                Err(e) => return Err(error::CommandError::new_from_error(Box::new(e))),
            },
            Err(e) => return Err(error::CommandError::new_from_error(Box::new(e))),
        }
    }
}
