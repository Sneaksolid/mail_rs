use std::error;
use std::fmt;

#[derive(Debug)]
pub struct CommandError {
    msg: String,
}

impl CommandError {
    #[allow(dead_code)]
    pub fn new(message: String) -> CommandError {
        CommandError { msg: message }
    }

    pub fn new_from_error(err: Box<error::Error>) -> CommandError {
        CommandError {
            msg: err.as_ref().description().to_string(),
        }
    }
}

impl error::Error for CommandError {
    fn description(&self) -> &str {
        &self.msg[..]
    }
}

impl fmt::Display for CommandError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "CommandError has happened")
    }
}
