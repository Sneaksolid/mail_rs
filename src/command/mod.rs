pub mod cmd;
pub mod error;

use super::helper_rs;
use super::imap_client;

pub trait Command {
    fn name<'a>(&self) -> &'a str;
    fn execute(
        &self,
        client: &mut imap_client::ImapClient,
        params: &Vec<String>,
    ) -> Result<(), error::CommandError>;
}
