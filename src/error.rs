use std::error;
use std::fmt;

#[derive(Debug)]
pub struct TerminalError {
    msg: String,
}

impl TerminalError {
    #[allow(dead_code)]
    pub fn new(message: String) -> TerminalError {
        TerminalError { msg: message }
    }
}

impl error::Error for TerminalError {
    fn description(&self) -> &str {
        &self.msg[..]
    }
}

impl fmt::Display for TerminalError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "TerminalError has happened")
    }
}

#[derive(Debug)]
pub struct ClientError {
    msg: String,
}

impl ClientError {
    pub fn new(message: String) -> ClientError {
        ClientError { msg: message }
    }

    pub fn new_from_error(err: Box<error::Error>) -> ClientError {
        ClientError {
            msg: err.as_ref().description().to_string(),
        }
    }
}

impl error::Error for ClientError {
    fn description(&self) -> &str {
        &self.msg[..]
    }
}

impl fmt::Display for ClientError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ClientError has happened")
    }
}
