extern crate atarashii_imap;
extern crate mailparse;
extern crate openssl;

use super::error;
use super::helper_rs;
use atarashii_imap::Response;

pub struct ImapClient {
    conn: atarashii_imap::Connection,
    current_mailbox: atarashii_imap::EmailBox,
}

#[allow(dead_code)]
impl ImapClient {
    pub fn open_connection(
        host: &str,
        user: &str,
        password: &str,
    ) -> Result<ImapClient, error::ClientError> {
        let mut connection: atarashii_imap::Connection;
        let mailbox: atarashii_imap::EmailBox;

        match atarashii_imap::Connection::open_secure(host, user, password) {
            Ok(conn) => connection = conn,
            Err(_) => {
                return Err(error::ClientError::new(String::from(
                    "couldn't establish connection",
                )))
            }
        }

        match connection.examine("inbox") {
            Ok(sel_res) => mailbox = sel_res,
            Err(_) => {
                return Err(error::ClientError::new(String::from(
                    "couldn't select the inbox",
                )))
            }
        }

        Ok(ImapClient {
            conn: connection,
            current_mailbox: mailbox,
        })
    }

    pub fn logout(mut self) -> Result<(), String> {
        match self.conn.logout() {
            Ok(_) => {}
            Err(_) => return Err(String::from("couldn't logout")),
        }
        Ok(())
    }

    pub fn print_most_recent_headers(&mut self, count: u32) -> Result<(), error::ClientError> {
        if let Err(e) = helper_rs::terminal::hl() {
            return Err(error::ClientError::new_from_error(Box::new(e)));
        }

        if count == 0 {
            return Ok(());
        }

        let headers = self.fetch_header_data(count, "From Subject Date")?;
        for header in headers {
            self.process_mail(header.0, &header.1)?;
        }
        if let Err(e) = helper_rs::terminal::hl() {
            return Err(error::ClientError::new_from_error(Box::new(e)));
        }
        Ok(())
    }

    fn fetch_header_data(
        &mut self,
        count: u32,
        fields: &str,
    ) -> Result<Vec<(u32, Vec<String>)>, error::ClientError> {
        let mut output: Vec<(u32, Vec<String>)> = Vec::new();

        if count == 0 {
            return Ok(output);
        }

        for i in
            (self.current_mailbox.exists_num - (count - 1))..(self.current_mailbox.exists_num + 1)
        {
            match self.conn.fetch(
                &i.to_string()[..],
                &("body[header.fields (".to_owned() + fields + ")]"),
            ) {
                Err(_) => {
                    return Err(error::ClientError::new(format!(
                        "Error fetching mail: {}",
                        i
                    )))
                }
                Ok(Response::Ok(data)) => output.push((i, data)),
                Ok(_) => return Err(error::ClientError::new(String::from("failed"))),
            }
        }

        Ok(output)
    }

    pub fn get_most_recent_headers_simple(
        &mut self,
        count: u32,
        max_column_count: u16,
    ) -> Result<Vec<String>, error::ClientError> {
        if count == 0 {
            return Ok(vec![]);
        }

        let mut output: Vec<String> = Vec::new();
        let headers = self.fetch_header_data(count, "From Subject")?;
        for header in headers {
            output.push(self.get_mail_header_string(header.0, &header.1, max_column_count)?);
        }

        Ok(output)
    }

    pub fn get_mail_text(&mut self, id: u32) -> Result<String, error::ClientError> {
        if id > self.current_mailbox.exists_num || id <= 0 {
            return Err(error::ClientError::new(String::from("id does not exist")));
        }

        match self.conn.fetch(&id.to_string()[..], "body[text]") {
            Err(_) => {
                return Err(error::ClientError::new(format!(
                    "Error fetching mail: {}",
                    id
                )))
            }
            Ok(Response::Ok(data)) => {
                let mut data_str = String::from("");
                for line in data.iter() {
                    if helper_rs::strings::starts_with(&line, "--------------")
                        || helper_rs::strings::starts_with(&line, "TAG")
                    {
                        break;
                    }
                    data_str += &line[..];
                    data_str += "\n";
                }
                match mailparse::parse_mail(data_str.as_bytes()) {
                    Ok(parsed_mail) => return Ok(parsed_mail.get_body().unwrap()),
                    Err(_) => return Ok(data_str),
                }
            }
            Ok(_) => return Err(error::ClientError::new(String::from("failed"))),
        };
    }

    fn process_mail(&mut self, id: u32, mail_data: &Vec<String>) -> Result<(), error::ClientError> {
        let mail_flags: String;
        match self.get_flags_of_mail(id) {
            Ok(s) => mail_flags = s,
            Err(_) => mail_flags = String::from(""),
        }

        return self.print_out_line(mail_data, id, mail_flags.contains("\\Seen"));
    }

    fn get_mail_header_string(
        &mut self,
        id: u32,
        mail_data: &Vec<String>,
        max_column_count: u16,
    ) -> Result<String, error::ClientError> {
        let mail_flags: String;
        match self.get_flags_of_mail(id) {
            Ok(s) => mail_flags = s,
            Err(_) => mail_flags = String::from(""),
        }

        return self.prepare_mail_line(
            mail_data,
            id,
            mail_flags.contains("\\Seen"),
            max_column_count,
        );
    }

    fn get_flags_of_mail(&mut self, id: u32) -> Result<String, error::ClientError> {
        match self.conn.fetch(&id.to_string()[..], "flags") {
            Err(_) => {
                return Err(error::ClientError::new(format!(
                    "Error fetching mail: {}",
                    id
                )))
            }
            Ok(Response::Ok(data)) => {
                if let Some(s) = data.get(0) {
                    return Ok(s.clone());
                } else {
                    return Err(error::ClientError::new(String::from("no flag received")));
                }
            }
            Ok(_) => return Err(error::ClientError::new(String::from("failed"))),
        }
    }

    fn print_out_line(
        &mut self,
        mail_data: &Vec<String>,
        line_nr: u32,
        seen: bool,
    ) -> Result<(), error::ClientError> {
        let (mail_id, from, subject, date) = self.get_header_lines(mail_data, line_nr, seen);
        if let Err(e) = helper_rs::terminal::println(
            '|',
            &vec![mail_id, from, subject, date],
            &vec![5, 40, 40, 15],
        ) {
            return Err(error::ClientError::new_from_error(Box::new(e)));
        }
        Ok(())
    }

    fn get_header_lines(
        &mut self,
        mail_data: &Vec<String>,
        line_nr: u32,
        seen: bool,
    ) -> (String, String, String, String) {
        let mut subject = String::from("");
        let mut from = String::from("");
        let mut date = String::from("");
        let mail_id: String;

        if seen {
            mail_id = line_nr.to_string();
        } else {
            mail_id = "*".to_owned() + &line_nr.to_string()[..] + "*";
        }

        if let Some(s) = self.find_correct_line(mail_data, "Subject:", false) {
            subject = s;
        }
        if let Some(s) = self.find_correct_line(mail_data, "From:", true) {
            from = s;
        }
        if let Some(s) = self.find_correct_line(mail_data, "Date:", true) {
            date = s;
        }

        (mail_id, from, subject, date)
    }

    fn prepare_mail_line(
        &mut self,
        mail_data: &Vec<String>,
        line_nr: u32,
        seen: bool,
        max_column_count: u16,
    ) -> Result<String, error::ClientError> {
        let (mail_id, from, subject, _) = self.get_header_lines(mail_data, line_nr, seen);
        match helper_rs::terminal::format_custom_width(
            '|',
            &vec![mail_id, from, subject],
            &vec![5, 45, 50],
            max_column_count,
        ) {
            Ok(s) => return Ok(s),
            Err(e) => return Err(error::ClientError::new_from_error(Box::new(e))),
        }
    }

    fn find_correct_line(
        &self,
        mail_data: &Vec<String>,
        search_for: &str,
        trim: bool,
    ) -> Option<String> {
        for line in mail_data {
            if helper_rs::strings::starts_with(line, search_for) {
                if trim {
                    return Some(self.trim_line_data(line.clone()));
                } else {
                    return Some(line.clone());
                }
            }
        }

        None
    }

    fn trim_line_data(&self, line_data: String) -> String {
        if let Some(i) = line_data.find(':') {
            return String::from(&line_data[(i + 2)..]);
        }

        line_data
    }
}
