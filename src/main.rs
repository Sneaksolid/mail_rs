extern crate helper_rs;

mod command;
mod error;
mod imap_client;

use std::error::Error;
use std::io;
use std::io::Write;

fn get_commands() -> Vec<Box<command::Command>> {
    vec![
        Box::new(command::cmd::Fetch::new()),
        Box::new(command::cmd::Read::new()),
    ]
}

fn get_and_execute_command(
    command_list: &Vec<Box<command::Command>>,
    client: &mut imap_client::ImapClient,
) -> Result<(), String> {
    print!("?> ");
    if let Err(_) = io::stdout().flush() {
        return Err(String::from("error while flushing the terminal"));
    }
    let mut input_line = String::new();
    if let Err(_) = io::stdin().read_line(&mut input_line) {
        return Err(String::from("error while reading line"));
    }

    let mut command_part = input_line.split_whitespace();
    if let Some(com) = command_part.next() {
        if &com[..] == "exit" {
            return Err(String::from("Bye Bye..."));
        }

        let mut params: Vec<String> = Vec::new();
        for i in command_part.clone() {
            params.push(i.to_owned());
        }
        for fc in command_list.iter() {
            if fc.name() == &com[..] {
                if let Err(e) = fc.execute(client, &params) {
                    println!("{}", e.description());
                }
            }
        }
    }

    Ok(())
}

fn main() {
    //replace dummy with real credentials
    let mut client = match imap_client::ImapClient::open_connection("dummy", "dummyBN", "dummyPW") {
        Ok(c) => c,
        Err(s) => {
            println!("{}", s);
            panic!("panic!");
        }
    };
    let cmds = get_commands();

    loop {
        if let Err(s) = get_and_execute_command(&cmds, &mut client) {
            println!("{}", s);
            break;
        }
    }

    client.logout().unwrap();
}
